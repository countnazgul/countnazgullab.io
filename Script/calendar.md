### Description

The most effective calendar script.
Two things can/must be changed - DateId field (to match the script one) and date format

### Code    
```
Calendar:
Load
  TempDate                                             as DateId, 
  date(TempDate, 'DD/MM/YYYY')                         as Date,
  week(TempDate)                                       as Week, 
  year(TempDate)                                       as Year, 
  monthname(TempDate)                                  as Month,
  day(TempDate)                                        as Day,
  'Q' & ceil(month(TempDate) / 3)                      as Quarter, 
  week(weekstart(TempDate)) & '-' & WeekYear(TempDate) as WeekYear,
  weekDay(TempDate)                                    as WeekDay 
;
Load
  date(mindate + IterNo()) as TempDate,
  maxdate
While
   mindate + IterNo() <= maxdate
;
Load
  min(FieldValue('DateId', recno()))-1 as mindate,
  max(FieldValue('DateId', recno()))   as maxdate
AutoGenerate
   FieldValueCount('DateId')
;
```

### References 

[Reference 1](http://qlikviewcookbook.com/2015/05/better-calendar-scripts/)
