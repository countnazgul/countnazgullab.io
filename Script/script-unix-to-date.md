#### Description
Convert Unix timestamps to date

For example: 1512130695 will result in 01/12/2017

#### Code    
```
Date( Floor( UnixTimeStampField / 86400 ) + 25569, 'DD/MM/YYYY' )
```

#### References 

[Qlik Community 1](https://community.qlik.com/thread/205518)