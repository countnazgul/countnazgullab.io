### Description
Convert Unix timestamps to timestamp

For example: 1512130695 will result in 01/12/2017

### Code    
```
Timestamp( ( taken_at_timestamp  / 86400 ) + 25569, 'DD/MM/YYYY HH:mm:ss' )
```

### References 

[Qlik Community 1](https://community.qlik.com/thread/205518)