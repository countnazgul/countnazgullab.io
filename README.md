# Qlik Snippets

This repo contains useful examples which are somehow related to Qlik development process. 

`Somehow related` means that not all code snippets/examples are for Qlik `script` or `expressions`. Some of the snippets will be related to other software and/or approaches. For example `Qlik-CLI`, `Qlik Core` docker examples etc.