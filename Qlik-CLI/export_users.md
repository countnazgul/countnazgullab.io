### Description

Use `Qlik-CLI` to export all available users in csv or console table

### Code    
```powershell
# Connect-Qlik -ComputerName my-sense-server-ip-or-name -TrustAllCerts

# Array which will contain the result
$displayObjects = [System.Collections.ArrayList]@()

# Table definition that defines which columns need to be returned as result
# Convert the result to JSON
$tableDef = (@{
        entity  = "User"
        columns = @(
            @{ name = "id"; columnType = "Property"; definition = "id" },
            @{ name = "name"; columnType = "Property"; definition = "name" },
            @{ name = "rule"; columnType = "Property"; definition = "rule"},
            @{name= "userDirectory"; columnType= "Property"; definition= "userDirectory"},
            @{name= "userId"; columnType= "Property"; definition= "userId"},
            @{name= "roles"; columnType= "Property"; definition= "roles"},
            @{name= "inactive"; columnType= "Property"; definition= "inactive"},
            @{name= "blacklisted"; columnType= "Property"; definition= "blacklisted"},
            @{name= "removedExternally"; columnType= "Property"; definition= "removedExternally"}            
        )
    }) | ConvertTo-Json

# Invoke the POST request
$tableResult = Invoke-QlikPost "/qrs/User/table" $tableDef #-Verbose

# Loop through the response row and push them to $displayObjects array 
foreach ($row in $tableResult.rows) {
        $rowData = New-Object PSObject -Property @{Type = $obj; ID = $row[0]; Name = $row[1]; Rule = $row[2]; userDirectory = $row[3]; UserId = $row[4]; Role = ""; Inactive = $row[6]; BlackListed = $row[7]; RemovedExternally = $row[8]}
        $displayObjects.Add($rowData) > $null
}

# Order the columns in the array
$displayObjects = $displayObjects | select-object ID, Name, UserId, userDirectory, Role, Inactive, BlackListed, RemovedExternally

# Save the result in csv format
$displayObjects | Export-Csv 'Roles.csv' -NoTypeInformation

# The result can be print in table format inside the console
#$displayObjects | Format-Table -AutoSize ID, Name, UserId, userDirectory, Role, Inactive, BlackListed, RemovedExternally
```


