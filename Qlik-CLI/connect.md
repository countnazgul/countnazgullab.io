### Description

Different options to connect with `Qlik-CLI`

### Code

* Localhost:
```
Connect-Qlik
```

* Specific Sense server name
```
Connect-Qlik -ComputerName 'server-name-or-ip'
```

* Accept all certificates (mainly targeting self-signed ones)

  ```
  Connect-Qlik -ComputerName 'server-name-or-ip' -TrustAllCerts
  ```

* With user/pass
```
Connect-Qlik -ComputerName 'server-name-or-ip' -TrustAllCerts -username domain\user
```

  or

  ```
  Connect-Qlik -ComputerName 'server-name-or-ip' -TrustAllCerts -UseDefaultCredentials
  ```


### References 

[GitHub](https://github.com/ahaydon/Qlik-Cli)
