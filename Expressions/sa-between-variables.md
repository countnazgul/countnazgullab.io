### Description
Using set analysis to filter the records between two variables

### Code    
```
sum( {< SalesDate = {">=$(vStartDate)<=$(vEndDate)"} >}  SalesAmount)
```

### References 

[Qlik Community 1](https://community.qlik.com/thread/232503)