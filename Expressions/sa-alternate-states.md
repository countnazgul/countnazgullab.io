### Description
Assign a field to same field in another state

### Code    
```
sum( {< Manager = Group1::Manager >} SalesAmount )
```

### References 

[livingqlikview.com](http://livingqlikview.com/livingqlik-roots-the-ultimate-qlikview-set-analysis-reference/)