### Description
Filter the dimension based on Set Analysis calculation

### Code    
```
sum( {< Region = {"=rank(avg(SalesAmount)) > 100"} >} SalesAmount )
```

### References 

[livingqlikview.com](http://livingqlikview.com/livingqlik-roots-the-ultimate-qlikview-set-analysis-reference/)