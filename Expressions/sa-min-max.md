### Description
Using `max()`/`min()` function inside `set analysis`.

### Code    
```
sum( {< Date = {"$(=Max(Date))"} >} SalesAmount )
```

### References 

[Qlik Community 1](https://community.qlik.com/thread/179869)