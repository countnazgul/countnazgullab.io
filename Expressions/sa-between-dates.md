### Description
Using set analysis to filter the records between two dates

### Code    
```
sum( {< SalesDate = {">=$(=min(StartDate))<=$(=max(EndDate))"} >} SalesAmount)
```

### References 

[Qlik Community 1](https://community.qlik.com/thread/33737)

[Qlik Community 2](https://community.qlik.com/blogs/qlikviewdesignblog/2015/09/28/dates-in-set-analysis)