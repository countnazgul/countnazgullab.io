### Description
Having same calculation but with different conditions can be simplified by using variables with arguments.
Unlimited numbers of arguments can be used. Variable definistion shouldn't start with = sign.
The arguments are defined in the variable with $x where x is incremental number.
When used the variable need to be evaluated and arguments are provided as function params:

`$(VariableName(param1, param2, param3, ...))`

### Code    

**vTemp variable definition:**

`sum( 100 + $1 + $2 + $3 )`

**Expression usage:**

` =$(vTemp(1,2,0))`

## Example

[Download](https://s3.eu-west-2.amazonaws.com/qlik-snippets/Variable_with_parameters.qvw)

### References 

[Qlik Community 1](https://community.qlik.com/thread/205922)

